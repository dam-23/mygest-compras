# myGest Compras

Nuestro departamento se encarga de gestionar las compras comprobando datos de hojas de cálculo. Los datos se dividen en la fecha de cada factura, 
IVA pagado y el total sin IVA de cada factura. Los datos se almacenaran en una hoja de cálculo aparte llamada compras-procesadas.xlsx, donde en 
cada línea aparecerá una de las facturas registradas en la hoja original.

Autores: Pablo Costoya y Pablo Yllanes.