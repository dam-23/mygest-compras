import openpyxl
from openpyxl.styles import PatternFill, Border, Side, Font, Alignment
from openpyxl.workbook import Workbook

borde_negro = Border(
    left=Side(style='thin', color='000000'),
    right=Side(style='thin', color='000000'),
    top=Side(style='thin', color='000000'),
    bottom=Side(style='thin', color='000000')
)

verde_oscuro = PatternFill(start_color='008000', end_color='008000', fill_type='solid')
verde_claro = PatternFill(start_color='90EE90', end_color='90EE90', fill_type='solid')


def colorTitulo(c_1, c_2, c_3):
    c_1.fill = verde_oscuro
    c_2.fill = verde_oscuro
    c_3.fill = verde_oscuro

    c_1.font = Font(color='FFFFFF')
    c_2.font = Font(color='FFFFFF')
    c_3.font = Font(color='FFFFFF')

    c_1.border = borde_negro
    c_2.border = borde_negro
    c_3.border = borde_negro

    c_1.alignment = Alignment(horizontal='center', vertical='center')
    c_2.alignment = Alignment(horizontal='center', vertical='center')
    c_3.alignment = Alignment(horizontal='center', vertical='center')
    pass


def colorDato(c_1, c_2, c_3):
    c_1.fill = verde_claro
    c_2.fill = verde_claro
    c_3.fill = verde_claro

    c_1.border = borde_negro
    c_2.border = borde_negro
    c_3.border = borde_negro

    c_1.alignment = Alignment(horizontal='center', vertical='center')
    c_2.alignment = Alignment(horizontal='center', vertical='center')
    c_3.alignment = Alignment(horizontal='center', vertical='center')
    pass


if __name__ == "__main__":

    workbookLeer = openpyxl.load_workbook('Resources/compras.xlsx')
    worksheetLeer = workbookLeer['facturas-compras']

    workbookCompras = Workbook()
    workbookCompras['Sheet'].title = "facturas"
    worksheetCompras = workbookCompras["facturas"]

    formatoFecha = worksheetLeer.cell(row=3, column=1).number_format
    formatoEuros = worksheetLeer.cell(row=3, column=5).number_format

    fila = 0

    for row in worksheetLeer.iter_rows(values_only="true"):
        fila += 1
        if fila < 3:
            c1 = worksheetCompras.cell(row=fila, column=1, value=row[0])
            c2 = worksheetCompras.cell(row=fila, column=2, value=row[4])
            c3 = worksheetCompras.cell(row=fila, column=3, value=row[5])

            colorTitulo(c1, c2, c3)

            if fila == 1:
                worksheetCompras.merge_cells(start_row=fila, start_column=1, end_row=fila, end_column=3)

        else:
            if row[0] is not None:
                fecha = row[0]
                total = row[4]
                iva = round(row[4] * worksheetLeer['I2'].value, 2)

                c1 = worksheetCompras.cell(row=fila, column=1, value=fecha)
                c2 = worksheetCompras.cell(row=fila, column=2, value=total)
                c3 = worksheetCompras.cell(row=fila, column=3, value=iva)

                c1.number_format = formatoFecha
                c2.number_format = formatoEuros
                c3.number_format = formatoEuros

                colorDato(c1, c2, c3)

    workbookCompras.save('Resources/compras-procesadas.xlsx')
    print("Completado.")
