import openpyxl
from openpyxl.workbook import Workbook


# import formulas

def formatoFecha(valor):
    fecha = valor.value.strftime("%d/%m/%Y")
    return fecha


if __name__ == "__main__":
    ruta = 'C:/Users/Pablo/Desktop/LICEO/SXE/myGest/myGest/facturas/compras.xlsx'
    # Aquí ponemos nuestra ruta para no tener que buscarla de nuevo
    wb = openpyxl.load_workbook(ruta, data_only=True)
    ws = wb["facturas-compras"]
    listaFechas = []
    listaTotalSenIVE = []
    listaIVE = []
    sumTotal = 0

    for col in ws.iter_cols(min_col=1, max_col=1, min_row=3, max_row=12):
        for cell in col:
            listaFechas.append(formatoFecha(cell))

    print(listaFechas)
    for col in ws.iter_cols(min_col=5, max_col=5, min_row=3, max_row=12):
        for cell in col:
            sumTotal += cell.value
            listaTotalSenIVE.append(cell.value)

    print(listaTotalSenIVE)
    for col in ws.iter_cols(min_col=6, max_col=6, min_row=3, max_row=12):
        for cell in col:
            listaIVE.append(cell.value)

    print(listaIVE)
    print(sumTotal)

    workbookCompras = Workbook()


